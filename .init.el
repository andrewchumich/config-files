(setq-default indent-tabs-mode nil)

(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(setq org-todo-keywords
      '((sequence "TODO" "STARTED" "|" "DONE")
        (sequence "HAVE" "LOW" "DON'T HAVE" "|" )
        ))

(setq org-adapt-indentation nil)
(setq org-directory "~/org")

(require 'package)
(setq package-archives
      '(("melpa" . "http://melpa.org/packages/")
        ("gnu" . "http://elpa.gnu.org/packages/")
        ("marmalade" . "http://marmalade-repo.org/packages/")
        ("org" . "http://orgmode.org/elpa/")))

(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-export-backends (quote (ascii html icalendar latex md odt deck)))
 '(package-selected-packages (quote (elfeed htmlize paredit org))))



(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


(setq calendar-latitude 43.6150)
(setq calendar-longitude -116.2023)
(setq calendar-location-name "Boise, ID")
(visual-line-mode)
